<?php

namespace App\Console\Commands;

use App\Order;
use App\Events\OrderCreated;
use Illuminate\Console\Command;

class CreateOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates some arbitrary orders and sends them to the firehose';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        while (true) {
            $order = factory(Order::class)->create();
            dump($order);
            event(new OrderCreated($order));
            sleep(5);
        }
    }
}
