<?php

namespace App\Events\OrderCreated;

use App\Events\OrderCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\Firehose\FirehoseServiceInterface;

class SendDataToFirehose
{
    private $firehoseProvider;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(FirehoseServiceInterface $firehoseProvider)
    {
        $this->firehoseProvider = $firehoseProvider;
    }

    /**
     * Handle the event.
     *
     * @param  OrderCreated  $event
     * @return void
     */
    public function handle(OrderCreated $event)
    {
        $this->firehoseProvider->publish([
            "event" => "orderCreated",
            "data" => [
                "order" => [
                    "id" => $event->order->id,
                    "status" => $event->order->status,
                    "value" => $event->order->value,
                    "customer" => $event->order->customer->toArray()
                ]
            ]
        ]); 
    }
}
