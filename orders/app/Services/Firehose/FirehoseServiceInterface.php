<?php

namespace App\Services\Firehose;

interface FirehoseServiceInterface
{
	public function publish(array $data);
}