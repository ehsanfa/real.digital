<?php

namespace App\Services\Firehose;

use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use App\Services\Firehose\FirehoseServiceInterface;

class RabbitMqFirehoseProvider implements FirehoseServiceInterface
{
	private $connection;

	private function connect()
	{
		$this->connection = new AMQPStreamConnection(
			env('FIREHOSE_RABBIT_HOST'), 
			env('FIREHOSE_RABBIT_PORT'), 
			env('FIREHOSE_RABBIT_USER'), 
			env('FIREHOSE_RABBIT_PASS')
		);
	}

	public function publish(array $data)
	{
		$this->connect();
		$exchangeName = env('FIREHOSE_RABBIT_EXCHANGE');
		$channel = $this->connection->channel();
		$channel->exchange_declare($exchangeName, "fanout");
		$channel->basic_publish(
			new AMQPMessage(json_encode($data)), 
			$exchangeName
		);
		$channel->close();
		$this->connection->close();
	}
}