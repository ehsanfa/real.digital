<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order;
use App\Customer;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
	$statues = ["PENDING", "CANCELLED", "SUCCEEDED"];
    return [
    	"value" => rand(1, 500),
    	"status" => $statues[array_rand($statues)],
    	"customer_id" => factory(Customer::class)
    ];
});
