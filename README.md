## About The Project
The following project consists of two services:
- A service which handles orders
- A service which is responsible for creating vouchers
The two services talk to each other through a RabbitMQ message broker "Firehose" which uses a fanout exchange to publish whatever message it receives to whichever queue that's listening on that particular exchange.

On the orders side there's a job running on 5s intervals to create arbitrary orders, save them in its own database and publish them via Firehose. 

On the vouchers side, there's a command listening to the exchange through it's own queue and on whichever of the message that meets ites defined criteria, creates a voucher containing customer's and order's info.

## Deployment
1. Create a .env file in the root of the project.
```
cp .env.example .env
```
2. Make any modifications if needed.
3.
```
docker-compose -f docker-compose.yaml -f docker-compose-env.yaml up 
```