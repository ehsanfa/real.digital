<?php

namespace App\Services\Firehose;

use Closure;

interface FirehoseServiceInterface
{
	public function listen(string $queueName, Closure $callback);
}