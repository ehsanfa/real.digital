<?php

namespace App\Services\Firehose;

use Closure;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use App\Services\Firehose\FirehoseServiceInterface;

class RabbitMqFirehoseProvider implements FirehoseServiceInterface
{
	private $connection;

	private function connect()
	{
		$this->connection = new AMQPStreamConnection(
			env('FIREHOSE_RABBIT_HOST'), 
			env('FIREHOSE_RABBIT_PORT'), 
			env('FIREHOSE_RABBIT_USER'), 
			env('FIREHOSE_RABBIT_PASS')
		);
	}

	public function listen(string $queueName, Closure $callback)
	{
		$this->connect();
		$exchangeName = env('FIREHOSE_RABBIT_EXCHANGE');
		$channel = $this->connection->channel();
		$channel->exchange_declare($exchangeName, "fanout");
		$channel->queue_declare($queueName, false, false, true, false);
		$channel->queue_bind($queueName, $exchangeName);
		$channel->basic_consume($queueName, '', false, true, false, false, function ($msg) use ($callback) {
			$callback(json_decode($msg->body, true));
		});
		while ($channel->is_consuming()) {
			$channel->wait();
		}

		$channel->close();
		$this->connection()->close();
	}
}