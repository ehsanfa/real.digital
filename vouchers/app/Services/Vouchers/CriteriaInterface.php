<?php

namespace App\Services\Vouchers;

use App\Voucher;

interface CriteriaInterface
{
    public function setData(array $data) :CriteriaInterface;

    public function match() :bool;

    public function generateVoucher() :Voucher;
}