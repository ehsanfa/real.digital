<?php

namespace App\Services\Vouchers;

use App\Voucher;
use App\Services\Vouchers\CriteriaMatcherInterface;

class VoucherCriteriaMatcher implements CriteriaMatcherInterface
{
    private $data;
    private $criterias = [];

    public function setCriteria(CriteriaInterface $criteria) :CriteriaMatcherInterface
    {
        $this->criterias[] = $criteria;
        return $this;
    }

    public function generateVoucher(array $data) :?Voucher
    {
        foreach ($this->criterias as $criteria) {
            $criteria->setData($data);
            if ($criteria->match()) {
                return $criteria->generateVoucher();
            }
        }
        return null;
    }
}