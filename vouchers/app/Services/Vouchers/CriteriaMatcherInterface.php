<?php

namespace App\Services\Vouchers;

use App\Voucher;
use App\Services\Vouchers\CriteriaInterface;

interface CriteriaMatcherInterface 
{
    public function setCriteria(CriteriaInterface $criteria) :CriteriaMatcherInterface;

    public function generateVoucher(array $data) :?Voucher;
}