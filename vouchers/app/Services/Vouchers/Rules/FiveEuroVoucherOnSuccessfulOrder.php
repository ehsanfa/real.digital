<?php

namespace App\Services\Vouchers\Rules;

use App\Voucher;
use App\Services\Vouchers\CriteriaInterface;
use App\Services\Vouchers\Rules\Toolbox\SchemaChecker;

class FiveEuroVoucherOnSuccessfulOrder implements CriteriaInterface
{
    use SchemaChecker;

    private $data;

    public function setData(array $data) :CriteriaInterface
    {
        $this->data = $data;
        return $this;
    }

    public function match() :bool
    {
        try {
            $this->checkSchema($this->data, [
                "event",
                "data" => [
                    "order" => [
                        "id",
                        "status",
                        "customer" => [
                            "email"
                        ]
                    ]
                ]
            ]);
        } catch (\Exception $e) {
            return false;
        }
        if (
            $this->data["event"] == "orderCreated"
            && $this->data["data"]["order"]["status"] == "SUCCEEDED"
            && $this->data["data"]["order"]["value"] > 100
        ) {
            return true;
        }
        return false;
    }

    public function generateVoucher() :Voucher
    {
        $voucher = new Voucher;
        $voucher->value = 5;
        $voucher->external_order_id = $this->data["data"]["order"]["id"];
        $voucher->customer_email = $this->data["data"]["order"]["customer"]["email"];
        $voucher->criteria = get_class($this);
        return $voucher;
    }
}
