<?php

namespace App\Services\Vouchers\Rules\Toolbox;

trait SchemaChecker
{
    public function checkSchema($data, $schema)
    {
        if (!is_array($data)) return;
        array_walk($schema, function($item, $key) use ($data, $schema) {
            if (!is_array($item)) {
                $key = $item;
            }
            if (!isset($data[$key])) {
                throw new \Exception ("$key is not implemented");
            }
            $this->checkSchema($data[$key], $item);
        });
    }
}