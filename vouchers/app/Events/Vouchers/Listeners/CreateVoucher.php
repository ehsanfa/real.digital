<?php

namespace App\Events\Vouchers\Listeners;

use App\Events\Vouchers\NewVoucherDetected;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateVoucher
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewVoucherDetected  $event
     * @return void
     */
    public function handle(NewVoucherDetected $event)
    {
        $event->voucher->save();
    }
}
