<?php

namespace App\Events\Vouchers;

use App\Voucher;

class NewVoucherDetected
{
    public $voucher;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Voucher $voucher)
    {
        $this->voucher = $voucher;
    }
}
