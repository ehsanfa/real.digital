<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Vouchers\VoucherCriteriaMatcher;
use App\Services\Vouchers\CriteriaMatcherInterface;

class VoucherServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            CriteriaMatcherInterface::class,
            VoucherCriteriaMatcher::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
