<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Firehose\FirehoseServiceInterface;
use App\Services\Firehose\RabbitMqFirehoseProvider;

class FirehoseProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            FirehoseServiceInterface::class,
            RabbitMqFirehoseProvider::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
