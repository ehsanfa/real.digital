<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Events\Vouchers\NewVoucherDetected;
use App\Services\Firehose\FirehoseServiceInterface;
use App\Services\Vouchers\CriteriaMatcherInterface;
use App\Services\Vouchers\Rules\FiveEuroVoucherOnSuccessfulOrder;

class GenerateVouchers extends Command
{
    const CRITERIAS = [
        FiveEuroVoucherOnSuccessfulOrder::class
    ];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vouchers:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates vouchers by listening to firehose';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(
        FirehoseServiceInterface $firehose,
        CriteriaMatcherInterface $criteriaMatcher
    )
    {
        $firehose->listen(
            "generate_vouchers", 
            function ($data) use ($criteriaMatcher) {
                foreach (self::CRITERIAS as $criteria) {
                    $criteria = new $criteria;
                    $criteria->setData($data);
                    $criteriaMatcher->setCriteria($criteria);
                }
                if (!$voucher = $criteriaMatcher->generateVoucher($data)) {
                    dump("no voucher criteria found for this information");
                    return;
                }
                event(new NewVoucherDetected($voucher));
                dump("created voucher", $voucher);
            }
        );
    }
}
